Check True.
Check tt.
Check False.
Check not.

Theorem a_implies_not_not_a ( A : Prop): A -> not (not A).
Proof.
  intro aProof. compute. intro notAProof. exact (notAProof aProof).
Qed.

Theorem demorgan (A B : Prop) : not (A \/ B) -> not A /\ not B.
Proof.
  compute. intro notAorB.
  constructor. intro aProof. apply notAorB. apply or_introl. exact aProof.
  intro bProof. apply notAorB. apply or_intror. exact bProof.
Qed.

(* Proof the other de-morgan as an exercise *)

(* Natural numbers *)
Check nat.
Check 0.
Check S. 

Fixpoint plus (m n : nat) : nat :=
  match m with
    | 0    => n
    | S mp => S (plus mp n)
  end.

Theorem leftid (n : nat) : plus 0 n = n.
Proof.
  trivial.
Qed.

(* 

Theorem rightid (n : nat) : plus n 0 = n.
Proof.
  trivial.
Qed.
*)
