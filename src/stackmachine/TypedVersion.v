(* This is the typed version of the calculator. From Chapter 2 of cpdt *)

Require Import Nat.
Require Import Bool.
Require Import List.

Inductive type : Set := Nat | Bool.


Definition typeDenote (t : type) : Set :=
  match t with
    | Nat  => nat
    | Bool => bool
  end.

Check typeDenote.

Inductive tbinop : type -> type -> type -> Set :=
| tPlus            : tbinop Nat Nat Nat
| tMul             : tbinop Nat Nat Nat
| tLe              : tbinop Nat Nat Bool
| tEq   {t : type} : tbinop t   t   Bool
.
Check tbinop Nat Nat Nat.
Check tPlus.
(*
Inductive texpr : type -> Set :=
| tNConst : nat  -> texpr Nat
| tBConst : bool -> texpr Bool
| tBinOp  {t1 t2 t3 : type} : (tbinop t1 t2 t3) -> texpr t1 -> texpr t2 -> texpr t3
.
 *)

Inductive texpr : type -> Set :=
| tConst {t : type} : typeDenote t  -> texpr t
| tBinOp  {t1 t2 t3 : type} : (tbinop t1 t2 t3) -> texpr t1 -> texpr t2 -> texpr t3
.

Check (tBinOp) (tPlus) (@tConst Nat 5) (@tConst Nat 4).
Check @tConst Nat 5.
Check tBinOp tPlus.

(* Now comes the stack machine. *)
 
(* The type of the stack *)
Definition tstack := list type.

(* The value stack *)
Fixpoint vstack (ts : tstack) : Set :=
  match ts with
    | nil       => unit
    | t :: ts   => typeDenote t * vstack ts
  end.


Inductive tinstr : tstack  -> tstack -> Set :=
| tpush {ts : tstack}{t : type}     : typeDenote t -> tinstr  ts (t :: ts)
| texec {ts : tstack}{s t r : type} : tbinop s t r -> tinstr (s :: t :: ts) (r :: ts)
.
Check @tpush.
Check tpush.
Check @tpush nil Nat.

Inductive tprogram : tstack -> tstack -> Set :=
| pNil   {s : tstack}        : tprogram s s
| pInstr  {s1 s2 s3 : tstack} : tinstr s1 s2 -> tprogram s2 s3  -> tprogram s1 s3
.

Fixpoint app_p {s t} {tp} (p : tprogram s t) : tprogram t tp -> tprogram s tp :=
  match p with
    | pNil       => fun pnew => pnew
    | pInstr i pp => fun pnew => pInstr i (app_p pp pnew)
  end.
Check @app_p.
Check @pInstr.
Notation "{{ x }}"  := (pInstr x pNil).
Notation "x +++ y"  := (app_p x y) (at level 80, right associativity).



(* meaning of the constructs of the language *)

Definition tbinopDenote {s t r : type}(op : tbinop s t r)
: typeDenote s -> typeDenote t -> typeDenote r
  :=
  match op (*in tbinop s t r return  typeDenote s -> typeDenote t -> typeDenote r*)
  with
    | tPlus    => plus
    | tMul     => mult
    | tLe      => Nat.leb
    | @tEq Nat => Nat.eqb
    | @tEq Bool => eqb
  end.

Fixpoint texprDenote {t : type}(e : texpr t) : typeDenote t :=
  match e with
    | tConst  c          => c
    | tBinOp  op e1 e2   => tbinopDenote op (texprDenote e1) (texprDenote e2)
  end.

Check texprDenote.
(* Evaluating programs in native/non-stack machine language*)
Eval simpl in texprDenote (tBinOp (tPlus) (@tConst Nat 5) (@tConst Nat 45)).
Eval simpl in texprDenote (tBinOp (tLe)  (@tConst Nat 55) (@tConst Nat 45)).

Definition tinstrDenote {s0 s1} (i : tinstr s0 s1)  :=
  match i in tinstr s0 s1 return vstack s0 -> vstack s1
  with
    | tpush  x  => fun vs  => (x, vs)
    | texec op
      => fun vs => let '(a,(b,vsp)) := vs in (tbinopDenote op a b , vsp)
  end.

Fixpoint tprogramDenote {s0 s1}(p : tprogram s0 s1) :=
  match p with
    | pNil       => fun vs => vs
    | pInstr i pp  => fun vs => tprogramDenote pp (tinstrDenote i vs)
  end.


(* meanings *)


(* The compiler *)

Fixpoint compile {t}(e : texpr t) : forall {ts : tstack},  tprogram ts (t :: ts) :=
  match e in texpr t return forall ts, tprogram ts (t :: ts) with
    | tConst c         => fun _ => {{ tpush c }}
    | tBinOp op e0 e1  => fun _ => compile e1  +++ compile e0  +++ {{ texec op }}
  end.
Check @compile.

Definition y := tBinOp (tMul) (@tConst Nat 5) (@tConst Nat 6).
Eval simpl in texprDenote y.
Eval simpl in compile y.
Eval compute in compile y.
Check tprogramDenote. 
Eval simpl in tprogramDenote (compile y).
Check compile y.

Ltac unfold_fold f := unfold f; fold f.

Ltac simplifier :=
  repeat (unfold_fold compile); repeat (unfold_fold texprDenote); simpl; autorewrite with core; repeat simpl; trivial.

Ltac induct_on_expr :=
  let e := fresh "e" in intro e; induction e.

Ltac crush :=
  try repeat  match goal with
            | [ H : ?T |- ?T ]             => exact H
            | [ |- forall _: texpr, _ ]     => induct_on_expr
            | [ |- context[(_ ++ _) ++ _]] => rewrite app_assoc_reverse
            | [ H : _ |- _ ]               => rewrite H
            | _ => intros; simplifier

         end.

Lemma  tprog_assoc : forall s1 s2 s3 (p1 : tprogram s1 s2) (p2 : tprogram s2 s3) (vs : vstack s1), tprogramDenote (p1 +++ p2) vs = tprogramDenote p2 (tprogramDenote p1 vs).
Proof.
  induction p1.
  intros.
  simpl.
  reflexivity.

  intros.
  simpl.
  rewrite IHp1.
  reflexivity.
Qed.

Hint Rewrite tprog_assoc.

Theorem compiler_is_better (t : type) : forall (e : texpr t) (ts : tstack) (s : vstack ts), tprogramDenote (compile e) s = (texprDenote e, s).
Proof.
  induction e.
  induction ts.
  crush.
  crush.
  crush.
  (*
  Non- Crushed Proof!
  intros.
  simpl.
  reflexivity.
  simpl.
  reflexivity.
  intros.
  simpl.
  rewrite tprog_assoc.
  rewrite tprog_assoc.
  simpl.
  rewrite IHe1.
  rewrite IHe2.
  reflexivity.
  *)
Qed.
(* Proof left as exercise *)

Hint Rewrite compiler_is_better.
Theorem compiler_is_correct : forall (t : type)(e : texpr t), tprogramDenote (@compile _ e nil) tt = (texprDenote e, tt).
Proof.
  induction e.
  induction t.
  crush.
  crush.
  crush.
  (*
  simpl.
  reflexivity.
  simpl.
  reflexivity.
  rewrite compiler_is_better.
  reflexivity.
  *)
Qed.

